package situacion;
import java.util.ArrayList;
public class Vehiculo 
{
    // Atributos
    private String marca;
    private String modelo;
    private String patente;    
    private long kilometraje;

    private ArrayList<String> motivoReparacion = new ArrayList<String>();

    // Metodos constructores
    public void setMarca(String marca)
    {
        this.marca = marca;
    }
    public void setModelo(String modelo)
    {
        this.modelo = modelo;
    }
    public void setPatente(String patente)
    {
        this.patente = patente;
    }
    public void setKilometraje(long kilometraje)
    {
        this.kilometraje = kilometraje;
    }

    public void setMotivoReparacion(String nuevoMotivoReparacion)
    {
        motivoReparacion.add(nuevoMotivoReparacion);
    }

    // Metodos getter
    public String getMarca()
    {
        return this.marca;
    }
    public String getModelo()
    {
        return this.modelo;
    }
    public String getPatente()
    {
        return this.patente;
    }
    public long getKilometraje()
    {
        return this.kilometraje;
    }

    public int cantidadMotivos()
    {
        return this.motivoReparacion.size();
    }

    public ArrayList<String> especificarMotivos()
    {
        return this.motivoReparacion;
    }
}
