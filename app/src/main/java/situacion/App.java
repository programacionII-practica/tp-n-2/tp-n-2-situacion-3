/*
Situación 3
Se desea gestionar las actividades dentro de un taller de reparación de automóviles. En el taller al
ingresar el vehículo para reparación se completa una ficha con los datos del propietario (DNI,
apellido y nombre, domicilio, teléfono) datos del automóvil (marca, modelo, patente,
kilometraje), e información declarada por el propietario que indique el/los motivos por el cual 
ingresa el vehículo. Una vez concluida la reparación se completa esta ficha con las tareas de
reparación efectuadas, los repuestos utilizados y el costo de mano de obra
*/
package situacion;

public class App {
    public static void main(String[] args) 
    {
        Cliente cliente = new Cliente();
        // Datos del cliente
        cliente.setNombre("Gustavo");
        cliente.setApellido("Contreras");
        cliente.setDNI(43995185);
        cliente.setDomicilio("Barrio 144 viviendas Santa Rosa 110");
        cliente.setNumeroTelefono(371142);
        // Datos del vehiculo al que se le realiza las reparaciones necesarias
        cliente.vehiculo.setMarca("Ford");
        cliente.vehiculo.setModelo("Fiesta");
        cliente.vehiculo.setPatente("UWU 404");
        cliente.vehiculo.setKilometraje(100000);
        // Motivos de reparacion. Pueden quitarse o agregarse utilizando "//"
        cliente.vehiculo.setMotivoReparacion("Chirrido al frenar");
        // cliente.vehiculo.setMotivoReparacion("Revision de cubierta");
        // cliente.vehiculo.setMotivoReparacion("Perdida de aceite");

        Taller taller = new Taller();

        taller.efectuarFicha(cliente);
    }
}
