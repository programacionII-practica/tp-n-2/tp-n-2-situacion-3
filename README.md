Se desea gestionar las actividades dentro de un taller de reparación de automóviles. En el taller al
ingresar el vehículo para reparación se completa una ficha con los datos del propietario (DNI,
apellido y nombre, domicilio, teléfono) datos del automóvil (marca, modelo, patente,
kilometraje), e información declarada por el propietario que indique el/los motivos por el cual ingresa el vehículo. Una vez concluida la reparación se completa esta ficha con las tareas de
reparación efectuadas, los repuestos utilizados y el costo de mano de obra.

NOTA: Los archivos de este repositorio fueron transferidos de un repositorio anterior https://gitlab.com/Gustavocon/tp-n-2-situacion-3
